
import allure
import requests

@allure.feature("宠物商店宠物信息接口测试")
class TestPetstore:
    def setup_class(self):
        self.base_url = "https://petstore.swagger.io/v2"
        self.add_pet_url = self.base_url + "/pet"
        self.find_pet_url = self.base_url + "/pet/findByStatus"
        self.pet_data = {
            "id": 10001,
            "category": {
              "id": 1,
              "name": "dog"
            },
            "name": "Tom",
            "photoUrls": [],
            "tags": [
              {
                "id": 1,
                "name": "tag1"
              }
            ],
            "status": "available"
        }
        self.find_params = {
            "status": "available"
        }

    @allure.story("新增宠物接口冒烟用例")
    def test_add_pet(self):
        '''
        新增宠物
        :return:
        '''
        with allure.step("发出新增宠物接口请求"):
            r = requests.post(self.add_pet_url, json=self.pet_data)
        with allure.step("获取新增宠物接口响应"):
            print(r.json())
        with allure.step("新增宠物接口断言"):
            assert r.status_code == 200

    @allure.story("查询宠物接口冒烟用例")
    def test_find_pet(self):
        '''
        查询宠物
        :return:
        '''
        with allure.step("发出查询宠物接口请求"):
            r = requests.get(self.find_pet_url, params=self.find_params)
        with allure.step("获取查询宠物接口响应"):
            print(r.json())
        with allure.step("查询宠物接口断言"):
            assert r.status_code == 200